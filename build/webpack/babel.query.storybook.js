module.exports = {
  presets: [
    'es2015',
    'react', // required by react-storybook
    'stage-0',
  ],
  plugins: [
    'transform-runtime',
  ],
};
