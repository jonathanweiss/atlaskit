// This is currently duplicated in shared-variables.less
export const globalCollapsedWidth = 60; // px
export const containerCollapsedWidth = 60; // px
export const expandedWidth = 280; // px
export const containerPaddingExpanded = 20; // px
