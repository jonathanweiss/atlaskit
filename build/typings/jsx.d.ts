declare namespace JSX {
  interface Element { }
  interface IntrinsicElements {
    div: any;
    style: any;
    fieldset: any;
    legend: any;
    pre: any;
    slot: any;
  }
}
