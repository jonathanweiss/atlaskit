import 'style!./host.less';

import ProfileCardResourced from './wc/pf-profilecard-resourced';
import ProfileCard from './wc/pf-profilecard';

export {
  ProfileCard,
  ProfileCardResourced,
};

export default ProfileCard;
