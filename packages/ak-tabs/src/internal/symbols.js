// ak-tabs symbols

const buttonContainer = Symbol('buttonContainer');
const focusOnRender = Symbol('focusOnRender');
const labelsContainer = Symbol('labelsContainer');
const labelProp = Symbol('labelProp');
const selectedProp = Symbol('selectedProp');

// ak-tabs-tab symbols

const tabDropdownItem = Symbol('tabDropdownItem');
const tabLabel = Symbol('tabLabel');

export {
  buttonContainer,
  focusOnRender,
  labelsContainer,
  tabDropdownItem,
  tabLabel,
  labelProp,
  selectedProp,
};
