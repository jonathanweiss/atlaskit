/**
* This event is emitted when the blanket is clicked.
* The emission of this event can be controlled by the {@link Blanket#clickable} property.
*
* @event Blanket#activate
*/
// eslint-disable-next-line import/prefer-default-export
export const activate = 'activate';
