import * as events from './index.events';
import Prop from './index.Prop';
import Theme from './index.Theme';
import themeable from './index.themeable';

export default Theme;
export { events, Prop, themeable };
