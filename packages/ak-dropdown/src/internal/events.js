export const changeBefore = 'changeBefore';
export const changeAfter = 'changeAfter';
export const openBefore = 'openBefore';
export const openAfter = 'openAfter';
export const closeBefore = 'closeBefore';
export const closeAfter = 'closeAfter';

export const item = Object.freeze({
  up: 'up',
  down: 'down',
  tab: 'tab',
  activated: 'activatedItem',
});

export const trigger = Object.freeze({
  activated: 'activatedTrigger',
});
