#!/usr/bin/env bash
set -e

npm config set progress false
npm config set color always
npm config set loglevel warn
