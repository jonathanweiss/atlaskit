const loading = Symbol('loading');
const error = Symbol('error');

export { loading, error };
