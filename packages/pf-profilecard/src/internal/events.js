const events = {
  action: 'action',
  success: 'success',
  error: 'error',
};

export default events;
