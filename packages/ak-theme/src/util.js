export const tagName = e => e.tagName.toLowerCase();
export const themeNameFromNode = e => tagName(e);
