module.exports.baseIconChunkName = '__base';

// NOTE context change (../glyph) is a breaking change, as the exports change
module.exports.glyphFolderName = 'glyph';

module.exports.tmpFolderName = 'tmp';

module.exports.maxWidth = 20;
module.exports.maxHeight = 20;

module.exports.fileEnding = '.svg';
