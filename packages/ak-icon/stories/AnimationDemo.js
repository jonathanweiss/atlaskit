import { vdom } from 'skatejs'; // eslint-disable-line no-unused-vars
import sample from 'lodash.sample';
import React from 'react';

import styles from './styles.less';
import componentStyles from '../src/shadow.less';


class AnimationDemo extends React.Component {
  constructor(props) {
    super(props);
    this.toggleAnimation = this.toggleAnimation.bind(this);
  }

  componentDidMount() {
    this.startAnimating();
    this.checkbox.checked = true;
  }

  componentWillUnmount() {
    this.stopAnimating();
  }

  randomIcon() {
    const Icon = sample(this.props.components);
    return <Icon className={componentStyles.locals.akIcon} />;
  }

  startAnimating() {
    this.timer = setInterval(() => this.forceUpdate(), 300);
  }

  stopAnimating() {
    clearInterval(this.timer);
  }

  toggleAnimation(e) {
    if (e.target.checked) {
      this.startAnimating();
    } else {
      this.stopAnimating();
    }
  }

  render() {
    return (
      <div>
        <input
          type="checkbox"
          id="animate"
          onChange={this.toggleAnimation}
          ref={elem => (this.checkbox = elem)}
        /> <label htmlFor="animate">Animate</label>
        <hr />
        <div className={styles.locals.container}>
          {this.randomIcon()}
          {this.randomIcon()}
          {this.randomIcon()}
          {this.randomIcon()}
          {this.randomIcon()}
          {this.randomIcon()}
          {this.randomIcon()}
          {this.randomIcon()}
        </div>
      </div>
    );
  }
}
AnimationDemo.displayName = 'AnimationDemo';
AnimationDemo.propTypes = {
  components: React.PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
};
export default AnimationDemo;
