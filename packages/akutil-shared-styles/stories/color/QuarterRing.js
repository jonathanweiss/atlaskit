import React from 'react';

/* eslint-disable max-len */
const QuarterRing = props => (
  <svg width="100%" height="100%" viewBox="948 765 336 336" version="1.1" {...props}>
    <path d="M1150.033,765 C1150.033,876.58 1059.58,967.033 948,967.033 L948,1100.376 C1133.223,1100.376 1283.376,950.223 1283.376,765 L1150.033,765 Z" id="Fill-1" strokeOpacity="0.5" stroke="black" fill="currentColor" fillRule="evenodd" />
  </svg>
);
/* eslint-enable max-len */

export default QuarterRing;
