module.exports = () => ({
  partial: `${__dirname}/../partial/**/*.hbs`,
  helper: `${__dirname}/../helper/*.js`,
});
