const profile = Symbol('profile');
const fnRequest = Symbol('fnRequest');
const fnResolve = Symbol('fnResolve');
const fnReject = Symbol('fnReject');
const loading = Symbol('loading');
const failed = Symbol('failed');

export {
  profile,
  fnRequest,
  fnResolve,
  fnReject,
  loading,
  failed,
};
