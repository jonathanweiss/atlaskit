import reactify from 'akutil-react';

import CalendarIcon from 'ak-icon/glyph/confluence/calendar';
import CanvasIcon from 'ak-icon/glyph/confluence/canvas';
import PageIcon from 'ak-icon/glyph/confluence/page';
import PersonIcon from 'ak-icon/glyph/confluence/person';
import QuoteIcon from 'ak-icon/glyph/confluence/quote';
import DashboardIcon from 'ak-icon/glyph/bitbucket/dashboard';
import BitbucketIcon from 'ak-icon/glyph/bitbucket/logo';
import HelpIcon from 'ak-icon/glyph/help';
import QuestionIcon from 'ak-icon/glyph/question';
import SearchIcon from 'ak-icon/glyph/search';
import JiraIcon from 'ak-icon/glyph/jira/logo';
import CreateIcon from 'ak-icon/glyph/create';

export default {
  CalendarIcon: reactify(CalendarIcon),
  CanvasIcon: reactify(CanvasIcon),
  PageIcon: reactify(PageIcon),
  PersonIcon: reactify(PersonIcon),
  QuoteIcon: reactify(QuoteIcon),
  DashboardIcon: reactify(DashboardIcon),
  BitbucketIcon: reactify(BitbucketIcon),
  HelpIcon: reactify(HelpIcon),
  QuestionIcon: reactify(QuestionIcon),
  SearchIcon: reactify(SearchIcon),
  JiraIcon: reactify(JiraIcon),
  CreateIcon: reactify(CreateIcon),
};
