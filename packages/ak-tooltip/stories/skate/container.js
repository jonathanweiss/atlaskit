import { define, vdom } from 'skatejs'; // eslint-disable-line no-unused-vars
import React from 'react';

import AKTrigger from '../../src/index.tooltip-trigger';


/*
   This is a simple container element to show that we can bind a tooltip to an element in the
   shadowDOM.
*/
export default define('ak-container', {
  render() {
    const containerStyles = {
      height: '200px',
      width: '200px',
      border: '1px dashed red',
      position: 'relative',
    };
    const buttonStyles = {
      width: '80px',
      backgroundColor: 'orange',
      textAlign: 'center',
      position: 'absolute',
      bottom: '0px',
      right: '0px',
    };
    return (
      <div>
        <div style={containerStyles}>
          <AKTrigger style={buttonStyles} position="bottom" description="This is a tooltip">
            <span aria-describedby="ak-tooltip">Hover Me</span>
          </AKTrigger>
        </div>
      </div>
    );
  },
});
